class Movable extends GameEntity {
  constructor(x, y, size, orientation) {
    super(x, y, size, orientation)
  }

  move(direction) {
    switch (direction) {
      case ORIENTATION.LEFT: this.x -= 1; break;
      case ORIENTATION.UP: this.y -= 1; break;
      case ORIENTATION.DOWN: this.y += 1; break;
      case ORIENTATION.RIGHT: this.x += 1; break;
    }
  }
}
