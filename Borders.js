class Borders {
  constructor(i, j, cols, rows) {
    const rand = (Math.random() < .5)
    this.up = j === 0 ? true : false;
    this.left = i === 0 ? true : false;
    this.down = j === rows ? true : rand;
    this.right = i === cols ? true : !rand;
    this.x = i
    this.y = j

    this.breakableUp = j === 0 || !this.up ? false : Math.random() < .2;
    this.breakableLeft = i === 0 || !this.left ? false : Math.random() < .2;
    this.breakableDown = j === rows || !this.down ? false : Math.random() < .2;
    this.breakableRight = i === cols || !this.right ? false : Math.random() < .2;
  }

  draw(x, y, w, h) {
    let wallColor
    if (this.left) {
      if (this.breakableLeft) {
        wallColor = WALL_COLORS.BREAKABLE
      } else {
        wallColor = WALL_COLORS.SOLID
      }
      UtilDraw.line(x * w, y * h, x * w, (y + 1) * h, wallColor)
    }
    if (this.right) {
      if (this.breakableRight) {
        wallColor = WALL_COLORS.BREAKABLE
      } else {
        wallColor = WALL_COLORS.SOLID
      }
      UtilDraw.line((x + 1) * w, y * h, (x + 1) * w, (y + 1) * h, wallColor)
    }
    if (this.down) {
      if (this.breakableDown) {
        wallColor = WALL_COLORS.BREAKABLE
      } else {
        wallColor = WALL_COLORS.SOLID
      }
      UtilDraw.line(x * w, (y + 1) * h, (x + 1) * w, (y + 1) * h, wallColor)
    }
    if (this.up) {
      if (this.breakableUp) {
        wallColor = WALL_COLORS.BREAKABLE
      } else {
        wallColor = WALL_COLORS.SOLID
      }
      UtilDraw.line(x * w, y * h, (x + 1) * w, y * h, wallColor)
    }
  }

  setUp(bool) {
    if (this.y === 0) bool = true
    this.up = bool;
  }

  setDown(bool) {
    if (this.y === COLS - 1) bool = true
    this.down = bool;
  }

  setLeft(bool) {
    if (this.x === 0) bool = true
    this.left = bool;
  }

  setRight(bool) {
    if (this.x === ROWS - 1) bool = true
    this.right = bool;
  }

  getUp() {
    return this.up;
  }

  getDown() {
    return this.down;
  }

  getLeft() {
    return this.left;
  }

  getRight() {
    return this.right;
  }

  setBreakableUp(bool) {
    if (this.y === 0) bool = true
    this.breakableUp = bool;
  }

  setBreakableDown(bool) {
    if (this.y === COLS - 1) bool = true
    this.breakableDown = bool;
  }

  setBreakableLeft(bool) {
    if (this.x === 0) bool = true
    this.breakableLeft = bool;
  }

  setBreakableRight(bool) {
    if (this.x === ROWS - 1) bool = true
    this.breakableRight = bool;
  }

  isBreakable(orientation) {
    switch (orientation) {
      case ORIENTATION.LEFT:
        return this.breakableLeft
      case ORIENTATION.RIGHT:
        return this.breakableRight
      case ORIENTATION.UP:
        return this.breakableUp
      case ORIENTATION.DOWN:
        return this.breakableDown
    }
    return false
  }

  break(orientation) {
    switch (orientation) {
      case ORIENTATION.LEFT:
        this.setBreakableLeft(false)
        this.setLeft(false)
        break;
      case ORIENTATION.RIGHT:
        this.setBreakableRight(false)
        this.setRight(false)
        break;
      case ORIENTATION.UP:
        this.setBreakableUp(false)
        this.setUp(false)
        break;
      case ORIENTATION.DOWN:
        this.setBreakableDown(false)
        this.setDown(false)
        break;
    }
  }
}
