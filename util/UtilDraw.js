class UtilDraw {
  constructor() { }

  static line(x1, y1, x2, y2, colors, borderColors) {
    if (!borderColors) borderColors = colors
    stroke(...borderColors);
    fill(...colors);
    line(x1, y1, x2, y2);
  }

  static circle(x, y, diameter, colors, borderColors) {
    if (!borderColors) borderColors = colors
    stroke(...borderColors);
    fill(...colors);
    circle(x, y, diameter);
  }
}
