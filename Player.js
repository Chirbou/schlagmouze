class Player extends Movable {
  constructor(x, y, size, orientation) {
    super(x, y, size, orientation);
  }

  draw() {
    const offset = SIZE / 2
    const centerX = this.x * SIZE + offset
    const centerY = this.y * SIZE + offset
    UtilDraw.circle(centerX, centerY, this.size, PLAYER_COLORS.BODY.NORMAL)

    let angleEye1, angleEye2;

    switch (this.orientation) {
      case ORIENTATION.LEFT:
        angleEye1 = -Math.PI * 3 / 4
        angleEye2 = Math.PI * 3 / 4
        break;
      case ORIENTATION.RIGHT:
        angleEye1 = -Math.PI / 4
        angleEye2 = Math.PI / 4
        break;
      case ORIENTATION.UP:
        angleEye1 = -Math.PI / 4
        angleEye2 = -Math.PI * 3 / 4
        break;
      case ORIENTATION.DOWN:
        angleEye1 = Math.PI * 3 / 4
        angleEye2 = Math.PI / 4
        break;
    }
    const xAngleEye1 = Math.cos(angleEye1)
    const yAngleEye1 = Math.sin(angleEye1)
    const xAngleEye2 = Math.cos(angleEye2)
    const yAngleEye2 = Math.sin(angleEye2)

    UtilDraw.circle(centerX + xAngleEye1 * offset / 2,
      centerY + yAngleEye1 * offset / 2,
      this.size / 6,
      PLAYER_COLORS.EYES.NORMAL)
    UtilDraw.circle(centerX + xAngleEye2 * offset / 2,
      centerY + yAngleEye2 * offset / 2,
      this.size / 6,
      PLAYER_COLORS.EYES.NORMAL)
  }
}
