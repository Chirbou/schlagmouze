class GameEntity {
  constructor(x, y, size, orientation) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.orientation = orientation
  }

  collides(gameEntity) {
    return gameEntity.getX() === this.x
      && gameEntity.getY() === this.y
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  setOrientation(orientation) {
    this.orientation = orientation
  }

  getOrientation() {
    return this.orientation
  }
}
