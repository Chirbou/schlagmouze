const COLS = 12
const ROWS = 12
const SIZE = 100;

const ORIENTATION = {
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
  UP: 'UP',
  DOWN: 'DOWN',
}
const WALL_COLORS = {
  BREAKABLE: [255, 255, 0],
  SOLID: [0],
}
const DIAMOND_COLOR = {
  NORMAL: [200, 200, 255],
  BORDER: [100, 100, 255],
  PICKED_UP: [0, 0, 255],
}
const POWER_UP_COLOR = {
  NORMAL: [255, 200, 200],
  BORDER: [255, 100, 100],
}
const PLAYER_COLORS = {
  BODY: {
    NORMAL: [255],
  },
  EYES: {
    NORMAL: [255, 0, 0]
  },
}
