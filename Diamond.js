class Diamond extends GameEntity {
  constructor() {
    super(
      Math.floor(Math.random() * ROWS),
      Math.floor(Math.random() * COLS),
      SIZE / 2,
      ORIENTATION.LEFT
    );
  }

  draw() {
    const offset = SIZE / 2
    const centerX = this.x * SIZE + offset
    const centerY = this.y * SIZE + offset
    UtilDraw.circle(centerX, centerY, this.size, DIAMOND_COLOR.NORMAL, DIAMOND_COLOR.BORDER)
  }
}
