

class Board {

  constructor() {
    this.cols = COLS
    this.rows = ROWS
    this._initCells()
  }

  _initCells() {
    let topCell, leftCell;
    this.cells = [];
    for (let i = 0; i < this.cols; i++) {
      this.cells.push([]);
      for (let j = 0; j < this.rows; j++) {
        const cell = new Cell(i, j, new Borders(i, j, this.cols - 1, this.rows - 1))

        if (i > 0 && j > 0) {
          leftCell = this.cells[i - 1][j];
          topCell = this.cells[i][j - 1];
          if (leftCell.getBorders().getRight()) {
            cell.getBorders().setLeft(true);
            if (leftCell.getBorders().isBreakable(ORIENTATION.RIGHT))
              cell.getBorders().setBreakableLeft(true);
          }
          if (topCell.getBorders().getDown()) {
            cell.getBorders().setUp(true);
            if (topCell.getBorders().isBreakable(ORIENTATION.DOWN))
              cell.getBorders().setBreakableUp(true);
          }
        }

        this.cells[i].push(cell);

      }
    }
  }

  draw() {
    background(200)
    this._drawCells();
  }

  _drawCells() {
    for (let i in this.cells) {
      for (let j in this.cells[i]) {
        this.cells[i][j].draw();
      }
    }
  }

  getCell(x, y) {
    return this.cells[x][y]
  }

  toggleInvincible() {
    for (let i in this.cells) {
      for (let j in this.cells[i]) {
        this.cells[i][j].getBorders().setBreakableLeft(true);
        this.cells[i][j].getBorders().setBreakableRight(true);
        this.cells[i][j].getBorders().setBreakableUp(true);
        this.cells[i][j].getBorders().setBreakableDown(true);
      }
    }

    setTimeout(() => this._initCells(), 10000)
  }

  getNeighbour(x, y, orientation) {
    switch (orientation) {
      case ORIENTATION.LEFT:
        return this.cells[x - 1] ? this.cells[x - 1][y] : null
        break;
      case ORIENTATION.RIGHT:
        return this.cells[x + 1] ? this.cells[x + 1][y] : null
        break;
      case ORIENTATION.UP:
        return this.cells[x][y - 1] || null
        break;
      case ORIENTATION.DOWN:
        return this.cells[x][y + 1] || null
        break;
    }
  }
}
