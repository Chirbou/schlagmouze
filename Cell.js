class Cell {
  constructor(x, y, borders) {
    this.size = SIZE
    this.x = x
    this.y = y
    this.borders = borders;
  }

  draw() {
    this._drawBorders()
  }

  _drawBorders() {
    this.borders.draw(this.x, this.y, this.size, this.size)
  }

  getBorders() {
    return this.borders
  }
}
