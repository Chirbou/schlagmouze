const board = new Board()
const player = new Player(5, 5, SIZE / 2, ORIENTATION.LEFT)
const diamond = new Diamond()
let powerUp = new PowerUp()
let wallImg;
function preload() {
  wallImg = loadImage('./assets/wall.jpg'), img => image(img, 0, 0);
}

function setup() {
  frameRate(30)
  createCanvas(ROWS * SIZE + 1, COLS * SIZE + 1);
  initPeriodicEvents()
}

function draw() {
  board.draw()
  player.draw()
  diamond.draw()
  powerUp.draw()
  texture(wallImg);
}

function keyPressed() {
  handleMoves()
  handleAction()
  checkPowerUp()
  draw()
  checkGameResolve()
}

const initPeriodicEvents = () => {
  setInterval(() => powerUp = new PowerUp(), 10000)
}

const handleMoves = () => {
  const x = player.getX()
  const y = player.getY()
  const cell = board.getCell(x, y)

  if (keyCode === LEFT_ARROW) {
    player.setOrientation(ORIENTATION.LEFT)
    if (!cell.getBorders().getLeft())
      player.move(ORIENTATION.LEFT)
  }
  if (keyCode === RIGHT_ARROW) {
    player.setOrientation(ORIENTATION.RIGHT)
    if (!cell.getBorders().getRight())
      player.move(ORIENTATION.RIGHT)
  }
  if (keyCode === UP_ARROW) {
    player.setOrientation(ORIENTATION.UP)
    if (!cell.getBorders().getUp())
      player.move(ORIENTATION.UP)
  }
  if (keyCode === DOWN_ARROW) {
    player.setOrientation(ORIENTATION.DOWN)
    if (!cell.getBorders().getDown())
      player.move(ORIENTATION.DOWN)
  }
}

const checkPowerUp = () => {
  if (player.collides(powerUp)) {
    board.toggleInvincible()
    powerUp.hide()
  }
}
const checkGameResolve = () => {
  if (player.collides(diamond)) {
    alert('Bien jouet !')
    window.location.reload()
  }
}

const handleAction = () => {
  if (keyCode === 32) {
    const orientation = player.getOrientation()
    const x = player.getX()
    const y = player.getY()
    const cell = board.getCell(x, y)
    const neighbourUpCell = board.getNeighbour(x, y, ORIENTATION.UP)
    const neighbourLeftCell = board.getNeighbour(x, y, ORIENTATION.LEFT)
    const neighbourRightCell = board.getNeighbour(x, y, ORIENTATION.RIGHT)
    const neighbourDownCell = board.getNeighbour(x, y, ORIENTATION.DOWN)

    console.log(
      neighbourUpCell,
      neighbourLeftCell,
    )

    if (cell.getBorders().isBreakable(orientation)) {
      cell.getBorders().break(orientation)
      if (neighbourUpCell)
        neighbourUpCell.getBorders().break(ORIENTATION.DOWN)
      if (neighbourLeftCell)
        neighbourLeftCell.getBorders().break(ORIENTATION.RIGHT)
      if (neighbourRightCell)
        neighbourRightCell.getBorders().break(ORIENTATION.LEFT)
      if (neighbourDownCell)
        neighbourDownCell.getBorders().break(ORIENTATION.UP)
    }
  }
}
