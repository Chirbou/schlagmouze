class PowerUp extends Movable {
  constructor(type) {
    super(
      Math.floor(Math.random() * ROWS),
      Math.floor(Math.random() * COLS),
      SIZE / 2,
      ORIENTATION.LEFT
    );
    this.type = type
    this.hidden = false
  }

  draw() {
    if (this.hidden) return
    const offset = SIZE / 2
    const centerX = this.x * SIZE + offset
    const centerY = this.y * SIZE + offset
    UtilDraw.circle(centerX, centerY, this.size, POWER_UP_COLOR.NORMAL, POWER_UP_COLOR.BORDER)
  }

  hide() {
    this.hidden = true
  }

}
